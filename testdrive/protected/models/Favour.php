<?php

/**
 * This is the model class for table "DBMAIN.FAVOUR".
 *
 * The followings are the available columns in table 'DBMAIN.FAVOUR':
 * @property string $ID
 * @property string $NAME
 * @property string $NAME_D
 * @property string $DOCL_PROP_ID
 * @property string $DECLAR_PK_TYPE_ID
 * @property string $WHEN_CHANGED
 * @property string $WHEN_CREATED
 * @property string $WHO_CHANGED
 * @property string $WHO_CREATED
 * @property double $ORDER_LIST
 * @property string $YEAR_PK
 */
class Favour extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'DBMAIN.FAVOUR';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NAME, NAME_D, DECLAR_PK_TYPE_ID, WHEN_CHANGED, WHEN_CREATED, WHO_CHANGED, WHO_CREATED', 'required'),
			array('ORDER_LIST', 'numerical'),
			array('NAME, NAME_D', 'length', 'max'=>200),
			array('DOCL_PROP_ID', 'length', 'max'=>3),
			array('DECLAR_PK_TYPE_ID', 'length', 'max'=>2),
			array('WHO_CHANGED, WHO_CREATED', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, NAME, NAME_D, DOCL_PROP_ID, DECLAR_PK_TYPE_ID, WHEN_CHANGED, WHEN_CREATED, WHO_CHANGED, WHO_CREATED, ORDER_LIST, YEAR_PK', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'NAME' => 'Name',
			'NAME_D' => 'Name D',
			'DOCL_PROP_ID' => 'Docl Prop',
			'DECLAR_PK_TYPE_ID' => 'Declar Pk Type',
			'WHEN_CHANGED' => 'When Changed',
			'WHEN_CREATED' => 'When Created',
			'WHO_CHANGED' => 'Who Changed',
			'WHO_CREATED' => 'Who Created',
			'ORDER_LIST' => 'Order List',
			'YEAR_PK' => 'Year Pk',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('NAME',$this->NAME,true);
		$criteria->compare('NAME_D',$this->NAME_D,true);
		$criteria->compare('DOCL_PROP_ID',$this->DOCL_PROP_ID,true);
		$criteria->compare('DECLAR_PK_TYPE_ID',$this->DECLAR_PK_TYPE_ID,true);
		$criteria->compare('WHEN_CHANGED',$this->WHEN_CHANGED,true);
		$criteria->compare('WHEN_CREATED',$this->WHEN_CREATED,true);
		$criteria->compare('WHO_CHANGED',$this->WHO_CHANGED,true);
		$criteria->compare('WHO_CREATED',$this->WHO_CREATED,true);
		$criteria->compare('ORDER_LIST',$this->ORDER_LIST);
		$criteria->compare('YEAR_PK',$this->YEAR_PK,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbOracle;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Favour the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
