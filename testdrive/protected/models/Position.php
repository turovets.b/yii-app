<?php

/**
 * This is the model class for table "DBMAIN.POSITION".
 *
 * The followings are the available columns in table 'DBMAIN.POSITION':
 * @property double $ID
 * @property string $NAME
 * @property string $GROUP_CEL_ID
 * @property string $BLOCK_ID
 * @property double $POSITION_TYPE_ID
 * @property double $PLAN_ID
 * @property double $PLAN_NAB
 * @property string $RASP_TYPE
 * @property string $FL_NOT_EKZ
 * @property string $FL_ALTERNATIV
 * @property string $FL_CLOSE_POS
 * @property string $U_VUZ_ID
 * @property string $YEAR_PK
 * @property string $WHEN_CHANGED
 * @property string $WHEN_CREATED
 * @property string $WHO_CHANGED
 * @property string $WHO_CREATED
 * @property string $CODE
 *
 * The followings are the available model relations:
 * @property  * @property BLOCK $bLOCK
 * @property GROUPCEL $gROUPCEL
 * @property POSITIONTYPE $pOSITIONTYPE
 * @property UVUZ $uVUZ
 */
class Position extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'DBMAIN.POSITION';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('NAME, POSITION_TYPE_ID, PLAN_ID, PLAN_NAB, RASP_TYPE, FL_NOT_EKZ, FL_ALTERNATIV, FL_CLOSE_POS, U_VUZ_ID, YEAR_PK, WHEN_CHANGED, WHEN_CREATED, WHO_CHANGED, WHO_CREATED, CODE', 'required'),
			array('POSITION_TYPE_ID, PLAN_ID, PLAN_NAB', 'numerical'),
			array('NAME', 'length', 'max'=>150),
			array('GROUP_CEL_ID, BLOCK_ID, U_VUZ_ID', 'length', 'max'=>2),
			array('RASP_TYPE, FL_NOT_EKZ, FL_ALTERNATIV, FL_CLOSE_POS', 'length', 'max'=>1),
			array('YEAR_PK', 'length', 'max'=>4),
			array('WHO_CHANGED, WHO_CREATED', 'length', 'max'=>30),
			array('CODE', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, NAME, GROUP_CEL_ID, BLOCK_ID, POSITION_TYPE_ID, PLAN_ID, PLAN_NAB, RASP_TYPE, FL_NOT_EKZ, FL_ALTERNATIV, FL_CLOSE_POS, U_VUZ_ID, YEAR_PK, WHEN_CHANGED, WHEN_CREATED, WHO_CHANGED, WHO_CREATED, CODE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pLAN' => array(self::BELONGS_TO, '', 'PLAN_ID'),
			'bLOCK' => array(self::BELONGS_TO, 'BLOCK', 'BLOCK_ID'),
			'gROUPCEL' => array(self::BELONGS_TO, 'GROUPCEL', 'GROUP_CEL_ID'),
			'pOSITIONTYPE' => array(self::BELONGS_TO, 'POSITIONTYPE', 'POSITION_TYPE_ID'),
			'uVUZ' => array(self::BELONGS_TO, 'UVUZ', 'U_VUZ_ID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'NAME' => 'Name',
			'GROUP_CEL_ID' => 'Group Cel',
			'BLOCK_ID' => 'Block',
			'POSITION_TYPE_ID' => 'Position Type',
			'PLAN_ID' => 'Plan',
			'PLAN_NAB' => 'Plan Nab',
			'RASP_TYPE' => 'Rasp Type',
			'FL_NOT_EKZ' => 'Fl Not Ekz',
			'FL_ALTERNATIV' => 'Fl Alternativ',
			'FL_CLOSE_POS' => 'Fl Close Pos',
			'U_VUZ_ID' => 'U Vuz',
			'YEAR_PK' => 'Year Pk',
			'WHEN_CHANGED' => 'When Changed',
			'WHEN_CREATED' => 'When Created',
			'WHO_CHANGED' => 'Who Changed',
			'WHO_CREATED' => 'Who Created',
			'CODE' => 'Code',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('NAME',$this->NAME,true);
		$criteria->compare('GROUP_CEL_ID',$this->GROUP_CEL_ID,true);
		$criteria->compare('BLOCK_ID',$this->BLOCK_ID,true);
		$criteria->compare('POSITION_TYPE_ID',$this->POSITION_TYPE_ID);
		$criteria->compare('PLAN_ID',$this->PLAN_ID);
		$criteria->compare('PLAN_NAB',$this->PLAN_NAB);
		$criteria->compare('RASP_TYPE',$this->RASP_TYPE,true);
		$criteria->compare('FL_NOT_EKZ',$this->FL_NOT_EKZ,true);
		$criteria->compare('FL_ALTERNATIV',$this->FL_ALTERNATIV,true);
		$criteria->compare('FL_CLOSE_POS',$this->FL_CLOSE_POS,true);
		$criteria->compare('U_VUZ_ID',$this->U_VUZ_ID,true);
		$criteria->compare('YEAR_PK',$this->YEAR_PK,true);
		$criteria->compare('WHEN_CHANGED',$this->WHEN_CHANGED,true);
		$criteria->compare('WHEN_CREATED',$this->WHEN_CREATED,true);
		$criteria->compare('WHO_CHANGED',$this->WHO_CHANGED,true);
		$criteria->compare('WHO_CREATED',$this->WHO_CREATED,true);
		$criteria->compare('CODE',$this->CODE,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbOracle;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Position the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
