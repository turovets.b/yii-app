<?php

$this->pageTitle=Yii::app()->name;
$encodedTitle = CHtml::encode($this->pageTitle); 
array_unshift($dbOrclList,"Выберите направление:");
?>


<?php echo TbHtml::beginFormTb(); ?>
        <fieldset>
            <legend>Особые права и преимущества при приеме на обучение</legend>
            <div style="text-align: center;">
                <?php echo TbHtml::label('№ абитуриента', 'id_abit'); ?>
                <?php echo TbHtml::textField('id_abit', '', array(
                    'class' => 'form-next-input',
                    'maxlength' => '3',
                    'pattern' => '\d',
                    ));
                ?>
            </div>
            <div id="wrapper">
            <div id="position-row">
            <?php echo TbHtml::label('Выберите направление', 'db-main-position'); ?>
            <?php echo TbHtml::dropDownList('db_main_position', '', $dbOrclList, array(
                'id' => 'db-main-position',
            ));?>
            </div>
            <div class="three-cols-container" id="three-cols-container">
            <?php echo TbHtml::alert(TbHtml::ALERT_COLOR_INFO, 'Для выбора нескольких преимуществ зажмите "Ctrl"'); ?>
            <div class="three-cols" id="three-cols">
            <div style="width: 33em;">
                <?php echo TbHtml::label('Cписок преимуществ при приеме на обучение:', 'db-main-favour'); ?>
                <?php echo TbHtml::dropDownList('db-main-favour', '', $dbOrclList2, array('multiple'=>true,'displaySize'=>'20'));?>
            </div>
            <div class="three-cols__second">
                <?php echo TbHtml::button('',array(
                    'class'=>"fa fa-angle-double-right",
                    'size' => TbHtml::BUTTON_SIZE_SMALL,
                    'id'=>"fa-angle-double-right",
                ));
                ?>
                <?php echo TbHtml::button('',array(
                    'class'=>"fa fa-angle-double-left",
                    'size' => TbHtml::BUTTON_SIZE_SMALL,
                    'id'=>"fa-angle-double-left",
                ));
                ?>
                </div>                              
                <div style="width: 33em;">
                <?php echo TbHtml::label('Выбранные преимущества при приеме на обучение:', 'select-list'); ?>
                <?php echo TbHtml::dropDownList('select-list','',array(),array('multiple'=> true,'displaySize' => '20'));?>
                </div>
            </div>
            <div style ="text-align:right;">       
            <?php echo TbHtml::submitButton('Готово'); ?>
            </div>
            </div>
            </div>
            </div>
            <script>
            // Animated appearance after id_abit is filled 
            $wrapper = $('#wrapper');
            $wrapper.hide();

            $('#id_abit').focus();
            $('#id_abit').on('input', function () {
                if ($(this).val().length === 3) {
                    $wrapper.show(700, function() {});
                } else {
                    $threecolsContainer.hide();
                    $wrapper.hide(700, function() {});
                }
            });

            // Animated appearance flexbox after db-main-position is choises 
            $threecolsContainer = $('#three-cols-container');
            $threecolsContainer.hide();
           $('#db-main-position').on('change', function () {
                $threecolsContainer.show(700,function(){});
            });
            
            // Move Items Between Two Select Lists
            var $sel_favour = $('#db-main-favour'); // 1
            var $sel_list = $('#select-list');      // 2
            
            $('#fa-angle-double-right').click(function(e){
                var $selected = $($sel_favour.selector + ' :selected');
                $($sel_list).append($($selected).clone());
                $($selected).remove();
            });
            $('#fa-angle-double-left').click(function(e){
                var $selectList = $($sel_list.selector + ' :selected');
                $($sel_favour).append($($selectList).clone());
                $($selectList).remove();
            });
            </script>
        </fieldset>
    <?php echo TbHtml::endForm(); ?>
