<?php 
	/* @var $this Controller */ 
	$encodedTitle = CHtml::encode($this->pageTitle);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">

	<?php Yii::app()->bootstrap->register(); ?>
	<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/styles-twbs-additional.css'); ?>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title><?php echo $encodedTitle; ?></title>

</head>

<body>

<div class="container" id="page">

	<header class="subhead" id="header">
	<?php $this->widget('bootstrap.widgets.TbNavbar', array(
	'brandLabel' => $encodedTitle,
	'display' => null, // default is static to top
	'collapse' => true,
	'items' => array(
			array(
				'class' => 'bootstrap.widgets.TbNav',
				'items' => array(
					array('label'=>'Выбор преимуществ', 'url'=>array('/site/favour')),
					array('label'=>'Index', 'url'=>array('/site/index')),
					array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
					array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)		
					),
				),
			),
	));
	?>
	</header> 
	 
    <?php if (isset($this->breadcrumbs)) :?>
        <?php $this->widget('bootstrap.widgets.TbBreadcrumb', array(
            'links' => $this->breadcrumbs,
        )); ?>
    <?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<footer class="footer" id="footer">
		<div>Копирайт &copy; <?php echo date('Y'); ?>, Богдан Туровец.</div>
        <div>Всё права зарезервированы.</div>
        <div><?php echo Yii::powered(); ?></div>
        <div>Yii версии <?php echo Yii::getVersion(); ?></div>
    </footer>

</div><!-- page -->

</body>
</html>
